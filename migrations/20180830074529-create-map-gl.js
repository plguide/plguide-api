'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('mapGLs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      id: {
        type: Sequelize.INTEGER
      },
      sg_id: {
        type: Sequelize.INTEGER
      },
      mg_id: {
        type: Sequelize.INTEGER
      },
      g_id: {
        type: Sequelize.INTEGER
      },
      cmp_id: {
        type: Sequelize.INTEGER
      },
      gl_code: {
        type: Sequelize.INTEGER
      },
      gl_name: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('mapGLs');
  }
};