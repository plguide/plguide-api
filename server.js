const express = require('express');
const session = require('express-session')
const sso = require('./SSO');
const app = express();
const bodyParser = require('body-parser')
const group = require('./controllers/group.controller');
const gl = require('./controllers/gl.controller');
const oauth = require('./auth/auth');
app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true
}));
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}))



app.get('/auth/callback', oauth.callbackHandler(), (req, res) => {
    res.redirect('http://localhost:4200');
});
app.delete('/group/:id',oauth.authentication(),group.deleteGroup);
app.post('/group/edit',group.editGroup);
app.post('/group/new',group.newGroup);
app.get('/maingroup', oauth.authentication(), group.ListAllGroup);
app.get('/getAllgl/:cmp', gl.getAllGl)
app.get('/getlogin', oauth.authentication(),(req, res) => {
   
})

app.listen(80);