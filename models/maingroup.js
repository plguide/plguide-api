'use strict';
module.exports = (sequelize, DataTypes) => {
  const mainGroup = sequelize.define('mainGroup', {
  
    mg_name: DataTypes.STRING
  }, {});
  mainGroup.associate = function(models) {
   this.hasMany(models.Group,{foreignKey: 'mg_id', sourceKey: 'id'})
  };
  return mainGroup;
};