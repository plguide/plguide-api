'use strict';
module.exports = (sequelize, DataTypes) => {
  const subGroup = sequelize.define('subGroup', {

    sg_name: DataTypes.STRING,
    g_id: DataTypes.INTEGER
  }, {});
  subGroup.associate = function (models) {
    this.belongsTo(models.Group, {
      foreignKey: 'g_id',
      targetKey: 'id'
    })
  };
  return subGroup;
};