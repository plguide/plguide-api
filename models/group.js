'use strict';
module.exports = (sequelize, DataTypes) => {
  const Group = sequelize.define('Group', {
    mg_id: DataTypes.INTEGER,
    group_name: DataTypes.STRING
  }, {paranoid:true});
  Group.associate = function (models) {
    this.belongsTo(models.mainGroup, {
      foreignKey: 'mg_id',
      targetKey: 'id'
    });
    this.hasMany(models.subGroup, {
      foreignKey: 'g_id',
      sourceKey: 'id'
    });
    // associations can be defined here
  };
  return Group;
};