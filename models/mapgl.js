'use strict';
module.exports = (sequelize, DataTypes) => {
  const mapGL = sequelize.define('mapGL', {
 
    sg_id: DataTypes.INTEGER,
    mg_id: DataTypes.INTEGER,
    g_id: DataTypes.INTEGER,
    cmp_id: DataTypes.INTEGER,
    gl_code: DataTypes.INTEGER,
    gl_name: DataTypes.STRING
  }, {});
  mapGL.associate = function(models) {
    // associations can be defined here
  };
  return mapGL;
};