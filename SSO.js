'use strict';

let http = require('http'),
    ssoHOST = '192.168.0.195', ssoPORT = 1903;

module.exports = function (aid, env) {
    this.renderSignPage = function (req, res) {
        let fullUrl = encodeURIComponent('http://' + req.get('host') + req.originalUrl);
        let frame = `<iframe src="http://${ssoHOST}:${ssoPORT}/sso/token/generate/${aid}?url=${fullUrl}"
            style="width: 100%; height: 100%; position: fixed; z-index: 999999; left: 0; top: 0; border: none; background:#fff"></iframe>`;

        res.setHeader('Content-Type', 'text/html');
        res.writeHead(res.statusCode);
        res.write(frame);
        res.end();
        return false;
        
    };

    function ajaxGetAuthentication(param, callback) {
        let httpPost = http.request({
            "host": ssoHOST,
            "port": ssoPORT,
            "path": `/sso/token/validator/${aid}`,
            "method": "POST",
            "headers": {
                "Content-Type": "application/json"
            }
        }, function (xhr) {
            let response = '';
            xhr.setEncoding('utf8');
            xhr.on('data', function (shunk) {
                response += shunk;
            });

            xhr.on('end', function () {
                if (xhr.statusCode == 200)
                    response = JSON.parse(response) || null;
                callback(response, xhr.statusCode, xhr);
            });
        });
        httpPost.write(JSON.stringify(param));
        httpPost.end();
    }

    this.getAuthentication = function (req, res, callback) {
        env = env || 'WEB_PAGE';
        if (!req.session.ssoToken) {
            if (env == 'WEB_PAGE') {
                if (req.body.ssoToken) {
                    req.session.ssoToken = req.body.ssoToken;
                    res.setHeader('Content-Type', 'text/html');
                    res.writeHead(res.statusCode);
                    res.write('<script> parent.location.reload(true); </script>');
                    res.end();
                    return false;
                } else {
                    return this.renderSignPage(req, res);
                }
            } else {
                res.sendStatus(401).end();
                return false;
            }
        }
        ajaxGetAuthentication({ssoToken: req.session.ssoToken},
            function (response, statusCode) {
                if (env == 'WEB_PAGE') {
                    switch (statusCode) {
                        case 200:
                            break;
                        case 400:
                            res.status(statusCode).send('<h1 style="margin:50px;">400 Bad Request</h1>').end();
                            return false;
                        case 401:
                            delete req.session.ssoToken;
                            res.setHeader('REFRESH', 1);
                            res.end();
                            return false;
                        case 403:
                            res.status(statusCode).send(response).end();
                            return false;
                        default:
                            res.sendStatus(404).end();
                            return false;
                    }
                } else {
                    if (statusCode >= 400) {
                        res.sendStatus(statusCode);
                        return false;
                    }
                }
                callback({
                    personDetail: JSON.parse(response.personDetail),
                    panelLogout: response.panelLogout
                });
                return true;
            }
        );
    };
};