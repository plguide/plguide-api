const db = require('../models/gl/db.connect')
const sql = db.sequelize;
exports.getAllGl = async (req, res) => {
   
        const result = await sql.query(`select reknr as GL_code,isNULL(oms25_0,oms25_1) as GL_name FROM [${req.params.cmp}].[dbo].grtbk gbk`);
        res.json(result[0]);

}