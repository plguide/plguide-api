const models = require('../models');

exports.ListAllGroup = async (req, res) => {
    const mainGroup = await models.mainGroup.findAll({
        include: [models.Group]
    });
    res.json(mainGroup);
}
exports.deleteGroup = async (req, res) => {
    const Group = await models.Group.destroy({
        where: {
            id: req.params.id
        }
    })
    res.json(Group);
}

exports.editGroup = async (req, res) => {
    const Group = await models.Group.update({
        group_name: req.body.name,
        mg_id: req.body.main_group
    }, {
        where: {
            id: req.body.g_id
        }
    })
    let response = Group == 1 ? {
        "Success": "แก้ไขข้อมูลเรียบร้อยแล้ว"
    } : {
        "Error": 'ไม่สามารถแก้ไข้ข้อมูลได้'
    }
    res.json(response);
}
exports.newGroup = async (req, res) => {
    const Group = await models.Group.create({
        mg_id: req.body.main_group,
        group_name: req.body.name
    })
    //  let  response = Group==1?{"Success":"แก้ไขข้อมูลเรียบร้อยแล้ว"}:{"Error":'ไม่สามารถแก้ไข้ข้อมูลได้'}
    res.json(Group);
}