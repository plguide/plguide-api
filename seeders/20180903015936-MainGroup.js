'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('Person', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */

    return queryInterface.bulkInsert('mainGroups', [{
       
        mg_name: "Revenue (รายได้)",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
       
        mg_name: "Cost of Goods sold & Expenses (ค่าใช้จ่าย)",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
       
        mg_name: "EBIT (กำไร ขาดทุน ก่อนค่าใช้จ่ายทางการเงิน และค่าใช้จ่ายภาษีเงินได้)",
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ]);
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
    return queryInterface.bulkDelete('mainGroups', null, {});
  }

};