'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('Person', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert('groups', [{
      group_name: 'Revenue from Sale (รายได้จากการขาย)',
      createdAt: new Date(),
      updatedAt: new Date(),
      mg_id: 1
    }, {
      group_name: 'Revenue from Project  (รายได้จากการให้บริการก่อสร้าง)',
      createdAt: new Date(),
      updatedAt: new Date(),
      mg_id: 1
    }, {
      group_name: 'Revenue from Services & Commission (รายได้ค่านายหน้า และบริการ)',
      createdAt: new Date(),
      updatedAt: new Date(),
      mg_id: 1
    } ,{
      group_name: 'รายได้เงินปันผลรับ',
      createdAt: new Date(),
      updatedAt: new Date(),
      mg_id: 1
    }, {
      group_name: 'กำไร จากอัตราแลกเปลี่ยน',
      createdAt: new Date(),
      updatedAt: new Date(),
      mg_id: 1
    }, {
      group_name: 'รายได้อื่น',
      createdAt: new Date(),
      updatedAt: new Date(),
      mg_id: 1
    }]);
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  }
};