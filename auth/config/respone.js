function respError(status = 404, statusCode, message) {
    let err = new Error();
    err.status = status;
    err.statusCode = statusCode;
    err.message = message;

    return err;
}

module.exports = {
    respError
};