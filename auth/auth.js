'use strict';

let
    factory = require('./config/config.json'),
    responseError = require('./config/respone').respError,
	
    OAuth20 = require('oauth').OAuth2,
    async = require('async'),
    request = require('request'),
		
    category = process.env.NODE_ENV === 'production' ? 'production' : 'otherwise',
    crypt = require('crypto'),
    md5 = crypt.createHash('md5').update(factory[category].clientId).digest('hex'),

    oauth20 = new OAuth20(factory[category].clientId, factory[category].clientSecret, factory[category].baseUri, factory[category].authorizationUri, factory[category].tokenUri),
    clientCredentials = {};

process.env.REVOKE_URL = factory[category].baseUri + factory[category].revokeUri;

function revokeAccessToken(req) {
    delete req.session[md5].access_token;
    return req.session[md5];
}

function revokeAllToken(req) {
    delete req.session[md5];
    return true;
}

function userInfo(authorizeToken) {
    return new Promise((resolve, reject) => {
        request.get({
            url: `${factory[category].baseUri}/user/info`,
            headers: {authorization: authorizeToken}
        }, serialize((err, data, response) => {
            if (err)
                return reject(err);

            resolve(data);
        }));
    });
}

function getUserInfo(req) {
    return userInfo(`Bearer ${req.oauth2_token.access_token}`)
}

function authentication(options = {autoSignIn: true}) {
    return (req, res, next) => {
        req.session[md5] = req.session[md5] || {};
        let session = req.session[md5];
        if (!session.refresh_token) {
            if (options.autoSignIn) {
                let state = Date.now();
                session.state = state;
                let authUri = oauth20.getAuthorizeUrl({
                    response_type: 'code',
                    redirect_uri: factory[category].redirectUri,
                    scope: null,
                    state: state
                });
                res.send(html(authUri, 0, `Redirecting to OAuth2...`));
            } else {
                req.oauth2_token = null;
                next();
            }
        } else if (!session.access_token) {
            oauth20.getOAuthAccessToken(session.refresh_token, {grant_type: "refresh_token"}, (err, accessToken) => {
                if (err) {
                    delete req.session[md5];
                } else {
                    session.access_token = accessToken;
                    req.session[md5] = session;
                }
                res.send(html(null, 0, `Reloading token...`));
            });

        } else {
            req.oauth2_token = session;
            next();
        }
    }
}

function clientAuthentication() {
    return new Promise((resolve, reject) => {
        oauth20.getOAuthAccessToken('', {grant_type: 'client_credentials'}, function (err, accessToken, refreshToken, fullPacket) {
            if (err)
                reject(err);
            else
                resolve(fullPacket);
        });
    });
}

function getClientCredentials() {
    return new Promise((resolve, reject) => {
        let requestToken = false;
        async.waterfall([
            cb => {
                if (clientCredentials.hasOwnProperty('access_token')) {
                    if (clientCredentials.hasOwnProperty('expires_in') && +clientCredentials.expires_in < Date.now()) {
                        clientCredentials = {};
                        requestToken = true;
                        return cb();
                    }

                    tokenValidation()
                        .then(() => {
                            cb();
                        })
                        .catch(err => {
                            clientCredentials = {};
                            requestToken = true;
                            cb();
                        })
                } else {
                    requestToken = true;
                    cb();
                }
            },
            cb => {
                if (requestToken) {
                    clientAuthentication()
                        .then(token => {
                            clientCredentials = token;
                            cb();
                        })
                        .catch(err => {
                            cb(err);
                        });
                } else {
                    cb();
                }
            }
        ], err => {
            if (err)
                return reject(err);

            resolve(clientCredentials);
        })
    });
}

function getUser(email) {
    let query = ``;
    if (!!email)
        query = `?email=${encodeURIComponent(email.trim())}`;
    return new Promise((resolve, reject) => {
        getClientCredentials()
            .then(credentials => {
                request.get({
                    url: `${factory[category].baseUri}/client/users${query}`,
                    headers: {authorization: `Bearer ${credentials.access_token}`}
                }, serialize((err, data, response) => {
                    if (err)
                        reject(err);
                    else
                        resolve(data);
                }));
            })
            .catch(reject);
    });
}

function updateUser(email, metadata) {
    let query = `?email=${email || ''}`;
    return new Promise((resolve, reject) => {
        getClientCredentials()
            .then(credentials => {
                request.put({
                    url: `${factory[category].baseUri}/client/users${query}`,
                    headers: {authorization: `Bearer ${credentials.access_token}`},
                    body: metadata,
                    json: true
                }, serialize((err, data, response) => {
                    if (err)
                        reject(err);
                    else
                        resolve(data);
                }))
            })
            .catch(reject);
    });
}

function postNewUser(data) {
    return new Promise((resolve, reject) => {
        getClientCredentials()
            .then(credentials => {
                request.post({
                    url: `${factory[category].baseUri}/client/users`,
                    headers: {authorization: `Bearer ${credentials.access_token}`},
                    body: data,
                    json: true
                }, serialize((err, data, response) => {
                    if (err)
                        reject(err);
                    else
                        resolve(data);
                }));
            })
            .catch(reject);
    });
}

function removeUser(email) {
    let query = ``;
    if (!!email)
        query = `?email=${encodeURIComponent(email.trim())}`;
    return new Promise((resolve, reject) => {
        getClientCredentials()
            .then(credentials => {
                request.delete({
                    url: `${factory[category].baseUri}/client/users${query}`,
                    headers: {authorization: `Bearer ${credentials.access_token}`}
                }, serialize((err, data, response) => {
                    if (err)
                        reject(err);
                    else
                        resolve(data);
                }))
            })
            .catch(reject);
    });
}

function tokenValidation(access_token) {
    return new Promise((resolve, reject) => {
        request.get({
            url: `${factory[category].baseUri}/validation`,
            headers: {authorization: 'Bearer ' + access_token}
        }, serialize((err, data, response) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        }));
    });
}

function callbackHandler() {
    return (req, res, next) => {
        let code,
            state;

        async.waterfall([
            cb => {
                if (req.query.error)
                    return cb(responseError(null, req.query.error, req.query.error_description));
                if (!req.session)
                    return cb(responseError(500, null, 'session not provided.'));
                let session = req.session[md5] || {};
                if (!session.state)
                    return cb(responseError(400, null, 'state not provided.'));
                if (+session.state !== +req.query.state)
                    return cb(responseError(400, null, 'invalid state.'));
                code = req.query.code;
                cb();
            },
            cb => {
                oauth20.getOAuthAccessToken(code, {grant_type: 'authorization_code'}, (err, access_token, refresh_token, result) => {
                    if (err) {
                        err.data = JSON.parse(err.data);
                        return cb(responseError(err.statusCode, err.data.error, err.data.error_description));
                    }
                    req.session[md5] = {
                        access_token: access_token,
                        refresh_token: refresh_token,
                        expires_in: Date.now() + (result.expires_in * 1000),
                        token_type: result.token_type
                    };
                    cb()
                });
            }
        ], err => {
            next(err);
        });
    }
}

function html(url, delay = 0, msg) {
    return `<html><head><meta http-equiv="refresh" content="${delay}; ${!!url ? 'url=' + url : ''}"></head><body style="margin: 50px"><h2>${msg}</h2></body></html>`
}

function serialize(callback) {
    return (err, response, body) => {
        if (!err)
            if (response.headers['content-type'].match('application/json')) {
                try {
                    body = JSON.parse(body);
                } catch (e) {
                }
            }
        // body = response.headers['content-type'].match('application/json') ? JSON.parse(body) : body;

        if (err || response.statusCode >= 400)
            return callback(err || body);

        callback(null, body, response);
    }
}

module.exports = {
    userInfo,
    getUser,
    authentication,
    callbackHandler,
    revokeAccessToken,
    revokeAllToken,
    factory: factory[category],
    getUserInfo,
    updateUser,
    postNewUser,
    removeUser,
    tokenValidation
};